﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebApplication7.Classes;


namespace WebApplication7
{
    public class CustomContext: DbContext
    {
        public CustomContext()
            : base("DbConnection")
        { }

        public DbSet<Detail> Details { get; set; }
        public DbSet<Accessory> Accessories { get; set; }
        public DbSet<Akk> Akkes { get; set; }
        public DbSet<Tip> Tips { get; set; }
        public DbSet<ATV> ATVs { get; set; }
        public DbSet<Body> Bodies { get; set; }
        public DbSet<Brend> Brends { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Drive> Drives { get; set; }
        public DbSet<Electricity> Electricity { get; set; }
        public DbSet<Engine> Engines { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Marki> Marks { get; set; }
        public DbSet<Marki_ATV> Marks_ATVs { get; set; }
        public DbSet<Marki_Snow> Marks_Snows { get; set; }
        public DbSet<Marki_Moto> Marks_Motos { get; set; }
        public DbSet<Moto> Motos { get; set; }
        public DbSet<Oil> Oils { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Snow> Snows { get; set; }
        public DbSet<Transmition> Transmitions { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
    }
}