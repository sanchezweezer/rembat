﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Classes
{
    public class Oil:Detail
    {
        public string Consist { get; set; }
        public string Naznachenie { get; set; }
        public int Volume { get; set; }
    }
}