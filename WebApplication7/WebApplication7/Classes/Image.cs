﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Classes
{
    public class Image
    {
        public int Id { get; set; }
        public byte[] Photo { get; set; }
    }
}