﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Classes
{
    public class Order
    {
        public int Id { get; set; }

        public virtual List<Detail> Details { get; set; }
        public virtual Customer Customers { get; set; }
    }
}