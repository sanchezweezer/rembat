﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Classes
{
    public class Detail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Articul { get; set; }
        public string Description { get; set; }

        public virtual Brend Brends { get; set; }
        public virtual Tip Tips { get; set; }
        //public virtual List<Vehicle> Vehicles { get; set; }
    }
}