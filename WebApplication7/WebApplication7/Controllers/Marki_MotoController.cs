﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class Marki_MotoController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Marki_Moto/
        public ActionResult Index()
        {
            return View(db.Marks_Motos.ToList());
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Id,Name")] Marki_Moto Marki)
        {
            return View("Moto", Marki);
        }

        // GET: /Marki_Moto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_Moto marki_moto = db.Marks_Motos.Find(id);
            if (marki_moto == null)
            {
                return HttpNotFound();
            }
            return View(marki_moto);
        }

        // GET: /Marki_Moto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Marki_Moto/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] Marki_Moto marki_moto)
        {
            if (ModelState.IsValid)
            {
                db.Marks.Add(marki_moto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marki_moto);
        }

        // GET: /Marki_Moto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_Moto marki_moto = db.Marks_Motos.Find(id);
            if (marki_moto == null)
            {
                return HttpNotFound();
            }
            return View(marki_moto);
        }

        // POST: /Marki_Moto/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] Marki_Moto marki_moto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marki_moto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marki_moto);
        }

        // GET: /Marki_Moto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_Moto marki_moto = db.Marks_Motos.Find(id);
            if (marki_moto == null)
            {
                return HttpNotFound();
            }
            return View(marki_moto);
        }

        // POST: /Marki_Moto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Marki_Moto marki_moto = db.Marks_Motos.Find(id);
            db.Marks.Remove(marki_moto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
