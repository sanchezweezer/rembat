﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class BrendController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Brend/
        public ActionResult Index()
        {
            return View(db.Marks.ToList());
        }

        // GET: /Brend/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brend brend = db.Brends.Find(id);
            if (brend == null)
            {
                return HttpNotFound();
            }
            return View(brend);
        }

        // GET: /Brend/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Brend/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] Brend brend)
        {
            if (ModelState.IsValid)
            {
                db.Marks.Add(brend);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brend);
        }

        // GET: /Brend/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brend brend = db.Brends.Find(id);
            if (brend == null)
            {
                return HttpNotFound();
            }
            return View(brend);
        }

        // POST: /Brend/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] Brend brend)
        {
            if (ModelState.IsValid)
            {
                db.Entry(brend).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(brend);
        }

        // GET: /Brend/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Brend brend = db.Brends.Find(id);
            if (brend == null)
            {
                return HttpNotFound();
            }
            return View(brend);
        }

        // POST: /Brend/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Brend brend = db.Brends.Find(id);
            db.Marks.Remove(brend);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
