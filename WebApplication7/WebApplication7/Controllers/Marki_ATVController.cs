﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class Marki_ATVController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Marki_ATV/
        public ActionResult Index()
        {
            return View(db.Marks_ATVs.ToList());
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Id,Name")] Marki_ATV Marki)
        {
            return View("ATV",Marki);
        }

        // GET: /Marki_ATV/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_ATV marki_atv = db.Marks_ATVs.Find(id);
            if (marki_atv == null)
            {
                return HttpNotFound();
            }
            return View(marki_atv);
        }

        // GET: /Marki_ATV/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Marki_ATV/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] Marki_ATV marki_atv)
        {
            if (ModelState.IsValid)
            {
                db.Marks.Add(marki_atv);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marki_atv);
        }

        // GET: /Marki_ATV/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_ATV marki_atv = db.Marks_ATVs.Find(id);
            if (marki_atv == null)
            {
                return HttpNotFound();
            }
            return View(marki_atv);
        }

        // POST: /Marki_ATV/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] Marki_ATV marki_atv)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marki_atv).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marki_atv);
        }

        // GET: /Marki_ATV/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_ATV marki_atv = db.Marks_ATVs.Find(id);
            if (marki_atv == null)
            {
                return HttpNotFound();
            }
            return View(marki_atv);
        }

        // POST: /Marki_ATV/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Marki_ATV marki_atv = db.Marks_ATVs.Find(id);
            db.Marks.Remove(marki_atv);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
