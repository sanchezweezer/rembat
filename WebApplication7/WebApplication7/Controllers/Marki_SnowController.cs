﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class Marki_SnowController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Marki_Snow/
        public ActionResult Index()
        {
            return View(db.Marks_Snows.ToList());
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Id,Name")] Marki_Snow Marki)
        {
            return View("Show", Marki);
        }

        // GET: /Marki_Snow/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_Snow marki_snow = db.Marks_Snows.Find(id);
            if (marki_snow == null)
            {
                return HttpNotFound();
            }
            return View(marki_snow);
        }

        // GET: /Marki_Snow/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Marki_Snow/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] Marki_Snow marki_snow)
        {
            if (ModelState.IsValid)
            {
                db.Marks.Add(marki_snow);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marki_snow);
        }

        // GET: /Marki_Snow/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_Snow marki_snow = db.Marks_Snows.Find(id);
            if (marki_snow == null)
            {
                return HttpNotFound();
            }
            return View(marki_snow);
        }

        // POST: /Marki_Snow/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] Marki_Snow marki_snow)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marki_snow).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marki_snow);
        }

        // GET: /Marki_Snow/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marki_Snow marki_snow = db.Marks_Snows.Find(id);
            if (marki_snow == null)
            {
                return HttpNotFound();
            }
            return View(marki_snow);
        }

        // POST: /Marki_Snow/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Marki_Snow marki_snow = db.Marks_Snows.Find(id);
            db.Marks.Remove(marki_snow);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
