﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class BodyController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Body/
        public ActionResult Index()
        {
            return View(db.Bodies.ToList());
        }

        // GET: /Body/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Body body = db.Bodies.Find(id);
            if (body == null)
            {
                return HttpNotFound();
            }
            return View(body);
        }

        // GET: /Body/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Body/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Price,Articul,Description")] Body body)
        {
            if (ModelState.IsValid)
            {
                db.Details.Add(body);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(body);
        }

        // GET: /Body/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Body body = db.Bodies.Find(id);
            if (body == null)
            {
                return HttpNotFound();
            }
            return View(body);
        }

        // POST: /Body/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Price,Articul,Description")] Body body, int Brends)
        {
            body.Brends = db.Brends.First(n => n.Id == Brends);
            
            if (ModelState.IsValid)
            {
                db.Entry(body).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(body);
        }

        // GET: /Body/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Body body = db.Bodies.Find(id);
            if (body == null)
            {
                return HttpNotFound();
            }
            return View(body);
        }

        // POST: /Body/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Body body = db.Bodies.Find(id);
            db.Details.Remove(body);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
