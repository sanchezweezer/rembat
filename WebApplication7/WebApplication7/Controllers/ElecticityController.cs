﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class ElecticityController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Electicity/
        public ActionResult Index()
        {
            return View(db.Electricity.ToList());
        }

        // GET: /Electicity/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Electricity electricity = db.Electricity.Find(id);
            if (electricity == null)
            {
                return HttpNotFound();
            }
            return View(electricity);
        }

        // GET: /Electicity/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Electicity/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Price,Articul,Description")] Electricity electricity)
        {
            if (ModelState.IsValid)
            {
                db.Details.Add(electricity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(electricity);
        }

        // GET: /Electicity/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Electricity electricity = db.Electricity.Find(id);
            if (electricity == null)
            {
                return HttpNotFound();
            }
            return View(electricity);
        }

        // POST: /Electicity/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Price,Articul,Description")] Electricity electricity, int Brends)
        {
            electricity.Brends = db.Brends.First(n => n.Id == Brends);
            
            if (ModelState.IsValid)
            {
                db.Entry(electricity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(electricity);
        }

        // GET: /Electicity/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Electricity electricity = db.Electricity.Find(id);
            if (electricity == null)
            {
                return HttpNotFound();
            }
            return View(electricity);
        }

        // POST: /Electicity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Electricity electricity = db.Electricity.Find(id);
            db.Details.Remove(electricity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
