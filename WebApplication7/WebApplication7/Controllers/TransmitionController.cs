﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class TransmitionController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Transmition/
        public ActionResult Index()
        {
            return View(db.Transmitions.ToList());
        }

        // GET: /Transmition/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transmition transmition = db.Transmitions.Find(id);
            if (transmition == null)
            {
                return HttpNotFound();
            }
            return View(transmition);
        }

        // GET: /Transmition/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Transmition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Price,Articul,Description")] Transmition transmition, int Brends)
        {
            transmition.Brends = db.Brends.First(n => n.Id == Brends);
            
            if (ModelState.IsValid)
            {
                db.Details.Add(transmition);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(transmition);
        }

        // GET: /Transmition/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transmition transmition = db.Transmitions.Find(id);
            if (transmition == null)
            {
                return HttpNotFound();
            }
            return View(transmition);
        }

        // POST: /Transmition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Price,Articul,Description")] Transmition transmition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transmition).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transmition);
        }

        // GET: /Transmition/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transmition transmition = db.Transmitions.Find(id);
            if (transmition == null)
            {
                return HttpNotFound();
            }
            return View(transmition);
        }

        // POST: /Transmition/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Transmition transmition = db.Transmitions.Find(id);
            db.Details.Remove(transmition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
