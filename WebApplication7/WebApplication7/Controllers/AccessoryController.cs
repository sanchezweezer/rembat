﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class AccessoryController : Controller
    {
        private CustomContext db = new CustomContext();
        private IEnumerable<Accessory> mm;

        // GET: /Accessory/
        public ActionResult Index(Vehicle M)
        {
            if (M.Id == 0)
            {
                return View(db.Accessories.ToList());
            }
            mm = db.Accessories.Where(n => n.Vehicles.Contains(M));

            return View(mm);
            
        }

        // GET: /Accessory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accessory accessory = db.Accessories.Find(id);
            if (accessory == null)
            {
                return HttpNotFound();
            }
            return View(accessory);
        }

        // GET: /Accessory/Create
        public ActionResult Create()
        {
            ViewBag.id_ttype = new SelectList(db.Brends, "Id", "Name");
            ViewBag.id_ttype1 = new MultiSelectList(db.Vehicles, "Id", "Name");
            return View();
        }

        // POST: /Accessory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Price,Articul,Description")] Accessory accessory, int id_ttype, List<int> id_ttype1)
        {
            accessory.Brends = db.Brends.First(n => n.Id == id_ttype);
            for (int i = 0; i >= id_ttype1.Count; i++ )
            {
                accessory.Vehicles.Add(db.Vehicles.First(n => n.Id == id_ttype1[i]));
                
            }

                if (ModelState.IsValid)
                {
                    db.Details.Add(accessory);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            return View(accessory);
        }

        // GET: /Accessory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accessory accessory = db.Accessories.Find(id);
            if (accessory == null)
            {
                return HttpNotFound();
            }
            return View(accessory);
        }

        // POST: /Accessory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Price,Articul,Description")] Accessory accessory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accessory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accessory);
        }

        // GET: /Accessory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accessory accessory = db.Accessories.Find(id);
            if (accessory == null)
            {
                return HttpNotFound();
            }
            return View(accessory);
        }

        // POST: /Accessory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Accessory accessory = db.Accessories.Find(id);
            db.Details.Remove(accessory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
