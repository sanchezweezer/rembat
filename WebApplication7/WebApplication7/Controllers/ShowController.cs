﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class ShowController : Controller
    {
        private CustomContext db = new CustomContext();
        private IEnumerable<Snow> mm;

        // GET: /Show/
        public ActionResult Index(Marki_Snow M)
        {
            if (M.Id == 0)
            {
                return View(db.Snows.ToList());
            }
            mm = db.Snows.Where(n => n.Marks_Snow.Id == M.Id);
            
            return View(mm);
        }

        // GET: /Show/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Snow snow = db.Snows.Find(id);
            if (snow == null)
            {
                return HttpNotFound();
            }
            return View(snow);
        }

        // GET: /Show/Create
        public ActionResult Create()
        {
            ViewBag.id_ttype = new SelectList(db.Marks_Snows, "Id", "Name");
            
            return View();
        }

        // POST: /Show/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Snow snow, int id_ttype)
        {
            snow.Marks_Snow = db.Marks_Snows.First(n => n.Id == id_ttype);
            
            if (ModelState.IsValid)
            {
                db.Vehicles.Add(snow);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(snow);
        }

        // GET: /Show/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Snow snow = db.Snows.Find(id);
            if (snow == null)
            {
                return HttpNotFound();
            }
            return View(snow);
        }

        // POST: /Show/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] Snow snow)
        {
            if (ModelState.IsValid)
            {
                db.Entry(snow).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(snow);
        }

        // GET: /Show/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Snow snow = db.Snows.Find(id);
            if (snow == null)
            {
                return HttpNotFound();
            }
            return View(snow);
        }

        // POST: /Show/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Snow snow = db.Snows.Find(id);
            db.Vehicles.Remove(snow);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
