﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class OilController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Oil/
        public ActionResult Index()
        {
            return View(db.Oils.ToList());
        }

        // GET: /Oil/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oil oil = db.Oils.Find(id);
            if (oil == null)
            {
                return HttpNotFound();
            }
            return View(oil);
        }

        // GET: /Oil/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Oil/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Price,Articul,Description,Consist,Naznachenie,Volume")] Oil oil, int Brends)
        {
            oil.Brends = db.Brends.First(n => n.Id == Brends);
            
            if (ModelState.IsValid)
            {
                db.Details.Add(oil);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oil);
        }

        // GET: /Oil/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oil oil = db.Oils.Find(id);
            if (oil == null)
            {
                return HttpNotFound();
            }
            return View(oil);
        }

        // POST: /Oil/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Price,Articul,Description,Consist,Naznachenie,Volume")] Oil oil)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oil).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oil);
        }

        // GET: /Oil/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oil oil = db.Oils.Find(id);
            if (oil == null)
            {
                return HttpNotFound();
            }
            return View(oil);
        }

        // POST: /Oil/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oil oil = db.Oils.Find(id);
            db.Details.Remove(oil);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
