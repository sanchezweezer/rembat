﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class AkkController : Controller
    {
        private CustomContext db = new CustomContext();

        // GET: /Akk/
        public ActionResult Index()
        {
            return View(db.Akkes.ToList());
        }

        // GET: /Akk/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Akk akk = db.Akkes.Find(id);
            if (akk == null)
            {
                return HttpNotFound();
            }
            return View(akk);
        }

        // GET: /Akk/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Akk/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Price,Articul,Description,Volume,Size")] Akk akk)
        {
            if (ModelState.IsValid)
            {
                db.Details.Add(akk);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(akk);
        }

        // GET: /Akk/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Akk akk = db.Akkes.Find(id);
            if (akk == null)
            {
                return HttpNotFound();
            }
            return View(akk);
        }

        // POST: /Akk/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Price,Articul,Description,Volume,Size")] Akk akk, int Brends)
        {
            akk.Brends = db.Brends.First(n => n.Id == Brends);
            
            if (ModelState.IsValid)
            {
                db.Entry(akk).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(akk);
        }

        // GET: /Akk/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Akk akk = db.Akkes.Find(id);
            if (akk == null)
            {
                return HttpNotFound();
            }
            return View(akk);
        }

        // POST: /Akk/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Akk akk = db.Akkes.Find(id);
            db.Details.Remove(akk);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
