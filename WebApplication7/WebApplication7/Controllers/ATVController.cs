﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication7.Classes;
using WebApplication7;

namespace WebApplication7.Controllers
{
    public class ATVController : Controller
    {
        private CustomContext db = new CustomContext();
        private IEnumerable<ATV> mm;

        // GET: /ATV/
        public ActionResult Index(Marki_ATV M)
        {
            if (M.Id == 0)
            {
                return View(db.ATVs.ToList());
            }
            mm = db.ATVs.Where(n => n.Marks_ATV.Id == M.Id);
            
            return View(mm);
            
        }

        // GET: /ATV/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ATV atv = db.ATVs.Find(id);
            if (atv == null)
            {
                return HttpNotFound();
            }
            return View(atv);
        }

        // GET: /ATV/Create
        public ActionResult Create()
        {
            ViewBag.Marks_ATV_Id = new SelectList(db.Marks_ATVs, "Id", "Name");
            
            return View();
        }

        // POST: /ATV/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] ATV atv, int Marks_ATV_Id)
        {
            atv.Marks_ATV = db.Marks_ATVs.First(n => n.Id == Marks_ATV_Id);
            if (ModelState.IsValid)
            {
                db.Vehicles.Add(atv);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(atv);
        }

        // GET: /ATV/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ATV atv = db.ATVs.Find(id);
            if (atv == null)
            {
                return HttpNotFound();
            }
            return View(atv);
        }

        // POST: /ATV/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] ATV atv)
        {
            if (ModelState.IsValid)
            {
                db.Entry(atv).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(atv);
        }

        // GET: /ATV/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ATV atv = db.ATVs.Find(id);
            if (atv == null)
            {
                return HttpNotFound();
            }
            return View(atv);
        }

        // POST: /ATV/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ATV atv = db.ATVs.Find(id);
            db.Vehicles.Remove(atv);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
