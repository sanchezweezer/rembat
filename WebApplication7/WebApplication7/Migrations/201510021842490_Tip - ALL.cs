namespace WebApplication7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TipALL : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Details", "All_Id", "dbo.All");
            DropForeignKey("dbo.Vehicles", "All_Id", "dbo.All");
            DropIndex("dbo.Details", new[] { "All_Id" });
            DropIndex("dbo.Vehicles", new[] { "All_Id" });
            CreateTable(
                "dbo.Tips",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Details", "Tips_Id", c => c.Int());
            AddColumn("dbo.Details", "Vehicle_Id", c => c.Int());
            CreateIndex("dbo.Details", "Tips_Id");
            CreateIndex("dbo.Details", "Vehicle_Id");
            AddForeignKey("dbo.Details", "Tips_Id", "dbo.Tips", "Id");
            AddForeignKey("dbo.Details", "Vehicle_Id", "dbo.Vehicles", "Id");
            DropColumn("dbo.Details", "All_Id");
            DropColumn("dbo.Vehicles", "All_Id");
            DropTable("dbo.All");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.All",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Vehicles", "All_Id", c => c.Int());
            AddColumn("dbo.Details", "All_Id", c => c.Int());
            DropForeignKey("dbo.Details", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Details", "Tips_Id", "dbo.Tips");
            DropIndex("dbo.Details", new[] { "Vehicle_Id" });
            DropIndex("dbo.Details", new[] { "Tips_Id" });
            DropColumn("dbo.Details", "Vehicle_Id");
            DropColumn("dbo.Details", "Tips_Id");
            DropTable("dbo.Tips");
            CreateIndex("dbo.Vehicles", "All_Id");
            CreateIndex("dbo.Details", "All_Id");
            AddForeignKey("dbo.Vehicles", "All_Id", "dbo.All", "Id");
            AddForeignKey("dbo.Details", "All_Id", "dbo.All", "Id");
        }
    }
}
