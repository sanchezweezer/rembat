namespace WebApplication7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Vehiclestodetails : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Details", "Detail_Id", "dbo.Details");
            DropIndex("dbo.Details", new[] { "Detail_Id" });
            AddColumn("dbo.Vehicles", "Detail_Id", c => c.Int());
            CreateIndex("dbo.Vehicles", "Detail_Id");
            AddForeignKey("dbo.Vehicles", "Detail_Id", "dbo.Details", "Id");
            DropColumn("dbo.Details", "Detail_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Details", "Detail_Id", c => c.Int());
            DropForeignKey("dbo.Vehicles", "Detail_Id", "dbo.Details");
            DropIndex("dbo.Vehicles", new[] { "Detail_Id" });
            DropColumn("dbo.Vehicles", "Detail_Id");
            CreateIndex("dbo.Details", "Detail_Id");
            AddForeignKey("dbo.Details", "Detail_Id", "dbo.Details", "Id");
        }
    }
}
