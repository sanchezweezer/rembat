namespace WebApplication7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Marki : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Vehicles", name: "Marks_Id", newName: "Marks_ATV_Id");
            RenameIndex(table: "dbo.Vehicles", name: "IX_Marks_Id", newName: "IX_Marks_ATV_Id");
            AddColumn("dbo.Vehicles", "Marks_Moto_Id", c => c.Int());
            AddColumn("dbo.Vehicles", "Marks_Snow_Id", c => c.Int());
            CreateIndex("dbo.Vehicles", "Marks_Moto_Id");
            CreateIndex("dbo.Vehicles", "Marks_Snow_Id");
            AddForeignKey("dbo.Vehicles", "Marks_Moto_Id", "dbo.Markis", "Id");
            AddForeignKey("dbo.Vehicles", "Marks_Snow_Id", "dbo.Markis", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vehicles", "Marks_Snow_Id", "dbo.Markis");
            DropForeignKey("dbo.Vehicles", "Marks_Moto_Id", "dbo.Markis");
            DropIndex("dbo.Vehicles", new[] { "Marks_Snow_Id" });
            DropIndex("dbo.Vehicles", new[] { "Marks_Moto_Id" });
            DropColumn("dbo.Vehicles", "Marks_Snow_Id");
            DropColumn("dbo.Vehicles", "Marks_Moto_Id");
            RenameIndex(table: "dbo.Vehicles", name: "IX_Marks_ATV_Id", newName: "IX_Marks_Id");
            RenameColumn(table: "dbo.Vehicles", name: "Marks_ATV_Id", newName: "Marks_Id");
        }
    }
}
