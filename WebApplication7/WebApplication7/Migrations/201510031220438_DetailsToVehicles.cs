namespace WebApplication7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DetailsToVehicles : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Details", "Vehicle_Id", "dbo.Vehicles");
            DropIndex("dbo.Details", new[] { "Vehicle_Id" });
            AddColumn("dbo.Details", "Detail_Id", c => c.Int());
            CreateIndex("dbo.Details", "Detail_Id");
            AddForeignKey("dbo.Details", "Detail_Id", "dbo.Details", "Id");
            DropColumn("dbo.Details", "Vehicle_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Details", "Vehicle_Id", c => c.Int());
            DropForeignKey("dbo.Details", "Detail_Id", "dbo.Details");
            DropIndex("dbo.Details", new[] { "Detail_Id" });
            DropColumn("dbo.Details", "Detail_Id");
            CreateIndex("dbo.Details", "Vehicle_Id");
            AddForeignKey("dbo.Details", "Vehicle_Id", "dbo.Vehicles", "Id");
        }
    }
}
