namespace WebApplication7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Details",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Int(nullable: false),
                        Articul = c.String(),
                        Description = c.String(),
                        Volume = c.Int(),
                        Size = c.String(),
                        Consist = c.String(),
                        Naznachenie = c.String(),
                        Volume1 = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Brends_Id = c.Int(),
                        All_Id = c.Int(),
                        Order_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Markis", t => t.Brends_Id)
                .ForeignKey("dbo.All", t => t.All_Id)
                .ForeignKey("dbo.Orders", t => t.Order_Id)
                .Index(t => t.Brends_Id)
                .Index(t => t.All_Id)
                .Index(t => t.Order_Id);
            
            CreateTable(
                "dbo.Markis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.All",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Marks_Id = c.Int(),
                        All_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Markis", t => t.Marks_Id)
                .ForeignKey("dbo.All", t => t.All_Id)
                .Index(t => t.Marks_Id)
                .Index(t => t.All_Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Time = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Photo = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Customers_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customers_Id)
                .Index(t => t.Customers_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Details", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "Customers_Id", "dbo.Customers");
            DropForeignKey("dbo.Vehicles", "All_Id", "dbo.All");
            DropForeignKey("dbo.Vehicles", "Marks_Id", "dbo.Markis");
            DropForeignKey("dbo.Details", "All_Id", "dbo.All");
            DropForeignKey("dbo.Details", "Brends_Id", "dbo.Markis");
            DropIndex("dbo.Orders", new[] { "Customers_Id" });
            DropIndex("dbo.Vehicles", new[] { "All_Id" });
            DropIndex("dbo.Vehicles", new[] { "Marks_Id" });
            DropIndex("dbo.Details", new[] { "Order_Id" });
            DropIndex("dbo.Details", new[] { "All_Id" });
            DropIndex("dbo.Details", new[] { "Brends_Id" });
            DropTable("dbo.Orders");
            DropTable("dbo.Images");
            DropTable("dbo.Customers");
            DropTable("dbo.Vehicles");
            DropTable("dbo.All");
            DropTable("dbo.Markis");
            DropTable("dbo.Details");
        }
    }
}
