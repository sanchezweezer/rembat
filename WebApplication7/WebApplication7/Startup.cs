﻿using Microsoft.Owin;
using Owin;
using WebApplication7.Classes;

[assembly: OwinStartupAttribute(typeof(WebApplication7.Startup))]
namespace WebApplication7
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            using (CustomContext db = new CustomContext())
            {
                // создаем два объекта User
                Detail user1 = new Detail { Name = "Tom" };
                Detail user2 = new Detail { Name = "Sam" };


                // добавляем их в бд

                db.SaveChanges();
                //Console.WriteLine("Объекты успешно сохранены");

                // получаем объекты из бд и выводим на консоль
                var det = db.Details;
                //Console.WriteLine("Список объектов:");
                
            }
        }
    }
}
